'use strict'


var vContainer = document.querySelector ('.container');
var vMenu = document.querySelector ('.menu');
var vOrder = document.querySelector ('.orders');
var vSearch = document.querySelector ('.search');
var btn1 = document.querySelector('.orderNameBtn')
var btn2 = document.querySelector('.orderIngIBtn');
var btn3 = document.querySelector('.orderIngEBtn')

//ingredience
const Ing = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Бекон',
    'Рыбная котлета',
    'Соус карри',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Капуста',
    'Кунжут',
    'Сыр Чеддер',
    'Сыр Виолла',
    'Сыр Гауда',
    'Майонез'
];

const BifIng = [
    'Булка',
    'Двойная супермясная котлета',
    'Бекон',
    'Соус Чили',
    'Лук',
    'Огурец маринованный',
    'Сыр',
    'Соус супер',
]

const СheeseIng = [
    'Булка',
    'Сыр Чеддер',
    'Сыр плавленный',
    'СуперСыр',
    'Говяжая котлета',
    'Соуст Тартар',
    'Помидорка',
    'Огурчик',
];

//basic menu and order
var menu = [];
var orders = [];
//goodburg is for burgers with or without some ing
var goodBurg = [];
var idNumber = 1;
var orderNumber = 100;

//burgers
class Burger {

    constructor ( name, ingredients, coockingTime, menuId ) {
        this.name = name;
        this.ingredients = ingredients;
        if (!coockingTime) {
            this.coockingTime = 30;
        } else {
            this.coockingTime = coockingTime;
        };
        this.menuId = idNumber;
        idNumber++;
    }

    showBurger () {
        console.log(`You create ${this.name}.
        Id: ${this.menuId}
        ingredients: ${this.ingredients}
        Waiting: ${this.coockingTime} minutes`);
    }

    addIng (smth) {
        this.ingredients.push(smth);
    }

    removeIng (smth) {
        var numOfIng = this.ingredients.indexOf(smth);
        if(numOfIng!=-1){
            this.ingredients.splice(numOfIng, 1);
            console.log(`We remomed ${smth} from ${this.name}`)
        } else {
            console.log (`In ${this.name} burger we dont have ${smth}`)
        };
    }

    toMenu () {
        menu.push({
            name: this.name,
            ingredients: this.ingredients,
            coockingTime: this.coockingTime,
            id: this.menuId,
        })
    }

};

//orders
class Orders {
    constructor (burger) {
        this.order = orderNumber++;
        orders.push(burger.name);
        console.log(`Your order of ${burger.name} will be ready in ${burger.coockingTime} minutes`);
    };
    make () {
        console.log (`${this.orderBurger.name} will be ready in ${this.orderBurger.coockingTime} minutes`);
    };
}
//Shows only existing in Menu burgers
function showMenu () {
    let newBurg = document.createElement('ul');
    vMenu.appendChild(newBurg);
    let menuBurg = (item) => {
        newBurg.insertAdjacentHTML('beforeEnd', `<li> ${item.id}:${item.name}<br>${item.ingredients}<br></br></li>`);
    };
    menu.forEach(menuBurg);
}

/*function currentMenu() {
    menu.forEach(item => {console.log(`You have ${item.name}. With ingrediance: ${item.ingrediance}`)});
};*/

var Cheese = new Burger ( 'Cheese', СheeseIng, 25);
Cheese.showBurger();
Cheese.toMenu();

var Bif = new Burger ( 'Bif', BifIng, 40);
Bif.showBurger();
Bif.toMenu();

var SuperClassic = new Burger ( 'SuperClassic', Ing );
SuperClassic.showBurger();
SuperClassic.toMenu();

//Order by name
function newOrderName() {
    var text1 = document.querySelector('.orderName');
    var ordName = text1.value;
    menu.forEach( item => {
        if(text1.value == item.name){
            var ordName = new Orders (item);
            console.log('Great choise');
            let ordBurg = document.createElement('ul');
            vOrder.appendChild(ordBurg);
            ordBurg.insertAdjacentHTML('beforeEnd', `<li> Your order ${item.name} is coocking. Coocking time is ${item.coockingTime} minutes</li>`);
        } else {
            console.log('Oops');
        };
    });
};

//order bt ing
function newOrderIngI() {
    var text2 = document.querySelector('.orderIngI');
    let goodBurg = [];
    vSearch.innerHTML = `With ${text2.value} you can choose:<br>`;
    menu.forEach( item => {
        if(item.ingredients.indexOf(text2.value) != -1){
            vSearch.insertAdjacentHTML('beforeEnd', `${item.name}<br>`);
            goodBurg.push(item);
        } else {
            console.log('no ing like this');
        };
    });
    if (goodBurg.length > 1) {
        let cur = goodBurg.length;
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }
        var numberForOrder = getRandomInt(0, cur);
        var ordName = new Orders (goodBurg[numberForOrder]);
        console.log('Great choise');
        let ordBurg = document.createElement('ul');
        vOrder.appendChild(ordBurg);
        ordBurg.insertAdjacentHTML('beforeEnd', `<li> Your order ${goodBurg[numberForOrder].name} is coocking. Coocking time is ${goodBurg[numberForOrder].coockingTime} minutes</li>`);
    } else if (goodBurg.length === 1){
        var ordName = new Orders (goodBurg[0]);
        console.log('Great choise');
        let ordBurg = document.createElement('ul');
        vOrder.appendChild(ordBurg);
        ordBurg.insertAdjacentHTML('beforeEnd', `<li> Your order ${goodBurg[0].name} is coocking. Coocking time is ${goodBurg[0].coockingTime} minutes</li>`);
    } else {
        vSearch.innerHTML = `We dont have any burger without ${text3.value}`
    };
};

//order without ing
function newOrderIngE() {
    var text3 = document.querySelector('.orderIngE');
    vSearch.innerHTML = `Without ${text3.value} you can choose:<br>`;
    let goodBurg = [];
    menu.forEach( item => {
        if(item.ingredients.indexOf(text3.value) === -1){
            //just show list of burgers
            vSearch.insertAdjacentHTML('beforeEnd', `${item.name}<br>`);
            //pushing burgers to list
            goodBurg.push(item);
        } else {
            console.log('no ing like this');
        };
    });
    // if we have a few burgers with correct ing
    if (goodBurg.length > 1) {
        let cur = goodBurg.length;
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }
        var numberForOrder = getRandomInt(0, cur);
        var ordName = new Orders (goodBurg[numberForOrder]);
        console.log('Great choise');
        let ordBurg = document.createElement('ul');
        vOrder.appendChild(ordBurg);
        ordBurg.insertAdjacentHTML('beforeEnd', `<li> Your order ${goodBurg[numberForOrder].name} is coocking. Coocking time is ${goodBurg[numberForOrder].coockingTime} minutes</li>`);
    } else if (goodBurg.length === 1){
        var ordName = new Orders (goodBurg[0]);
        console.log('Great choise');
        let ordBurg = document.createElement('ul');
        vOrder.appendChild(ordBurg);
        ordBurg.insertAdjacentHTML('beforeEnd', `<li> Your order ${goodBurg[0].name} is coocking. Coocking time is ${goodBurg[0].coockingTime} minutes</li>`);
    } else {
        vSearch.innerHTML = `We dont have any burger without ${text3.value}`
    }
};


btn1.addEventListener('click', newOrderName);
btn2.addEventListener('click', newOrderIngI);
btn3.addEventListener('click', newOrderIngE);

showMenu ();
